# fabricated-countyfips-age-schl

This repository holds a fabricated dataset in CSV format with 15,681,927 person-level observations of U.S. county, age, and educational attainment variables where:

 - `countyfips` is in five-digit Federal Information Processing Standards (FIPS) format with leading zeroes;
 - `age` is numerical; and
 - `schl` is categorical, using the current American Community Survey (ACS) variable label codes listed below.

To create this fabricated dataset, 15,681,927 observations were simulated from the current population distribution of `countyfips`. Then, age and educational attainment were assigned to `countyfips` randomly by drawing without replacement from the `age` and `schl` variables in the 2016, 5-year Public Use Microdata Series of the ACS. Thus, the joint distribution of `age` and `schl` in the fabricated file are similar to the national distribution. And because `age` and `schl` were assigned randomly to `countyfips`, county-conditional distributions of `age` and `schl` in the fabricated file are unlikely to reflect their population distributions. The data were generated with the Stata do file `fabricated-countyfips-age-schl.do`.

The variable labels for `schl` are as follows:

| Code | Label                                             |
|------|---------------------------------------------------|
| 01   | No schooling completed                            |
| 02   | Nursery school, preschool                         |
| 03   | Kindergarten                                      |
| 04   | Grade 1                                           |
| 05   | Grade 2                                           |
| 06   | Grade 3                                           |
| 07   | Grade 4                                           |
| 08   | Grade 5                                           |
| 09   | Grade 6                                           |
| 10   | Grade 7                                           |
| 11   | Grade 8                                           |
| 12   | Grade 9                                           |
| 13   | Grade 10                                          |
| 14   | Grade 11                                          |
| 15   | 12th grade - no diploma                           |
| 16   | Regular high school diploma                       |
| 17   | GED or alternative credential                     |
| 18   | Some college, but less than 1 year                |
| 19   | 1 or more years of college credit, no degree      |
| 20   | Associate's degree                                |
| 21   | Bachelor's degree                                 |
| 22   | Master's degree                                   |
| 23   | Professional degree beyond a bachelor's degree    |
| 24   | Doctorate degree                                  |
| BB   | N/A (less than 3 years old)                       |
